package no.uio.inf5750.assignment2.dao;

import no.uio.inf5750.assignment2.dao.hibernate.HibernateDegreeDAO;
import no.uio.inf5750.assignment2.model.Degree;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by martin on 24.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/assignment2/beans.xml" })
@Transactional
public class DegreeDAOTest {

    @Autowired
    DegreeDAO degreeDAO;

    Degree exampleDegree1;

    public void setDegreeDAO(DegreeDAO degreeDAO) {
        this.degreeDAO = degreeDAO;
    }

    @Before
    public void init() throws Exception {
        exampleDegree1 = new Degree("Super degree yeaah");
    }

    @Test
    public void testSaveDegree() {
        degreeDAO.saveDegree(exampleDegree1);
        Degree dbDegree = degreeDAO.getDegree(exampleDegree1.getId());

        Assert.assertNotNull(dbDegree);
    }

    @Test
    public void testGetDegree() {
        degreeDAO.saveDegree(exampleDegree1);
        Degree dbDegree = degreeDAO.getDegree(exampleDegree1.getId());

        boolean isTheSameDegree = exampleDegree1.equals(dbDegree);
        Assert.assertTrue(isTheSameDegree);
    }

    @Test
    public void testGetDegreeByType() {
        degreeDAO.saveDegree(exampleDegree1);
        Degree dbDegree = degreeDAO.getDegreeByType(exampleDegree1.getType());

        boolean isTheSameDegree = exampleDegree1.equals(dbDegree);
        Assert.assertTrue(isTheSameDegree);
    }

    @Test
    public void testGetAllDegrees() {
        Degree exampleDbDegree1 = new Degree("Bachelor 1");
        Degree exampleDbDegree2 = new Degree("Mastelor 2");
        Degree exampleDbDegree3 = new Degree("PhDlor 3");

        degreeDAO.saveDegree(exampleDbDegree1);
        degreeDAO.saveDegree(exampleDbDegree2);
        degreeDAO.saveDegree(exampleDbDegree3);

        boolean containsDbDegree1 = degreeDAO.getAllDegrees().contains(exampleDbDegree1);
        boolean containsDbDegree2 = degreeDAO.getAllDegrees().contains(exampleDbDegree2);
        boolean containsDbDegree3 = degreeDAO.getAllDegrees().contains(exampleDbDegree3);

        Assert.assertTrue(containsDbDegree1);
        Assert.assertTrue(containsDbDegree2);
        Assert.assertTrue(containsDbDegree3);
    }

    @Test
    public void testDelDegree() {
        degreeDAO.saveDegree(exampleDegree1);

        Degree dbDegreeBeforeDeletion = degreeDAO.getDegree(exampleDegree1.getId());
        Assert.assertNotNull(dbDegreeBeforeDeletion);

        degreeDAO.delDegree(exampleDegree1);

        Degree dbDegreeAfterDeletion = degreeDAO.getDegree(exampleDegree1.getId());
        Assert.assertNull(dbDegreeAfterDeletion);
    }
}
