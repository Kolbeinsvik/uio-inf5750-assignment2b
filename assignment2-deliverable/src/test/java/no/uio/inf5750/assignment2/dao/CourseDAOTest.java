package no.uio.inf5750.assignment2.dao;

import no.uio.inf5750.assignment2.dao.hibernate.HibernateCourseDAO;
import no.uio.inf5750.assignment2.model.Course;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by martin on 24.09.15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:/META-INF/assignment2/beans.xml" })
@Transactional
public class CourseDAOTest {

    @Autowired
    CourseDAO courseDAO;

    Course exampleCourse1;

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    @Before
    public void init() throws Exception {
        exampleCourse1 = new Course("INF1000", "INF tusen");
    }

    @Test
    public void testSaveCourse() {
        courseDAO.saveCourse(exampleCourse1);
        Course dbCourse = courseDAO.getCourse(exampleCourse1.getId());

        Assert.assertNotNull(dbCourse);
    }

    @Test
    public void testGetCourse() {
        courseDAO.saveCourse(exampleCourse1);
        Course dbCourse = courseDAO.getCourse(exampleCourse1.getId());

        boolean isTheSameCourse = exampleCourse1.equals(dbCourse);
        Assert.assertTrue(isTheSameCourse);
    }

    @Test
    public void testGetCourseByCourseCode() {
        courseDAO.saveCourse(exampleCourse1);
        Course dbCourse = courseDAO.getCourseByCourseCode(exampleCourse1.getCourseCode());

        boolean isTheSameCourse = exampleCourse1.equals(dbCourse);
        Assert.assertTrue(isTheSameCourse);
    }

    @Test
    public void testGetCourseByName() {
        courseDAO.saveCourse(exampleCourse1);
        Course dbCourse = courseDAO.getCourseByName(exampleCourse1.getName());

        boolean isTheSameCourse = exampleCourse1.equals(dbCourse);
        Assert.assertTrue(isTheSameCourse);
    }

    @Test
    public void testGetAllCourses() {
        Course exampleDbCourse1 = new Course("DBINF2001", "Database course 1");
        Course exampleDbCourse2 = new Course("DBINF2002", "Database course 2");
        Course exampleDbCourse3 = new Course("DBINF2003", "Database course 3");

        courseDAO.saveCourse(exampleDbCourse1);
        courseDAO.saveCourse(exampleDbCourse2);
        courseDAO.saveCourse(exampleDbCourse3);

        boolean containsDbCourse1 = courseDAO.getAllCourses().contains(exampleDbCourse1);
        boolean containsDbCourse2 = courseDAO.getAllCourses().contains(exampleDbCourse2);
        boolean containsDbCourse3 = courseDAO.getAllCourses().contains(exampleDbCourse3);

        Assert.assertTrue(containsDbCourse1);
        Assert.assertTrue(containsDbCourse2);
        Assert.assertTrue(containsDbCourse3);
    }

    @Test
    public void testDelCourse() {
        courseDAO.saveCourse(exampleCourse1);

        Course dbCourseBeforeDeletion = courseDAO.getCourse(exampleCourse1.getId());
        Assert.assertNotNull(dbCourseBeforeDeletion);

        courseDAO.delCourse(exampleCourse1);

        Course dbCourseAfterDeletion = courseDAO.getCourse(exampleCourse1.getId());
        Assert.assertNull(dbCourseAfterDeletion);
    }
}
