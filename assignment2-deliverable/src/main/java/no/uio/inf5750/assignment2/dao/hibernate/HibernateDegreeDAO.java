package no.uio.inf5750.assignment2.dao.hibernate;

import no.uio.inf5750.assignment2.dao.DegreeDAO;
import no.uio.inf5750.assignment2.model.Degree;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;

/**
 * Created by martin on 23.09.15.
 */
public class HibernateDegreeDAO implements DegreeDAO {
    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int saveDegree(Degree degree) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(degree);
        return degree.getId();
    }

    @Override
    public Degree getDegree(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Degree.class);
        criteria.add(Restrictions.eq("id", id));
        return (Degree) criteria.uniqueResult();
    }

    @Override
    public Degree getDegreeByType(String type) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Degree.class);
        criteria.add(Restrictions.eq("type", type));
        return (Degree) criteria.uniqueResult();
    }

    @Override
    public Collection<Degree> getAllDegrees() {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Degree.class);
        return (Collection<Degree>) criteria.list();
    }

    @Override
    public void delDegree(Degree degree) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(degree);
    }
}
